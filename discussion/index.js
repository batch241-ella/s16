console.log("Hello World!");

// ARITHMETIC OPERATORS
let x = 1397;
let y = 7831;

// Addition Operator
let sum = x + y;
console.log("Result of addition operator: " + sum);

// Subtraction Operator
let difference = x - y;
console.log("Result of the subtraction operator: " + difference);

// Multiplication Operator
let product = x * y;
console.log("Result of the multiplication operator: " + product);

// Division Operator
let quotient = x / y;
console.log("Result of the division operator: " + quotient);

// Modulo Operator 
let remainder = y % x;
console.log("Result of modulo operator: " + remainder);


// ASSIGNMENT OPERATOR
// Basic Assignment Operator (=)
// The assignment operator adds the value of the right operand to a variable and assigns the result to the variable
let assignmentNumber = 8;

// Addition Assignment Operator
// Uses the current value of the variable and it ADDS a number (e.g. 2) to itself. Afterwards, reassigns it as a new value.
assignmentNumber += 2;
console.log("This is the result of the addition assignment operator: " + assignmentNumber);

// Subtraction Assignment Operator
assignmentNumber -= 2;
console.log("Result of the subtraction assignment operator: " + assignmentNumber);

// Multiplication Assignment Operator
assignmentNumber *= 2;
console.log("Result of the multiplication assignment operator: " + assignmentNumber);

// Division Assignment Operator
assignmentNumber /= 2;
console.log("Result of the division assignment operator: " + assignmentNumber);

// Multiple Operators and Parentheses
/*
	When multiple operators are applied in a single statement, it follows the PEMDAS Rule (Parenthesis, Exponents, Multiplication, Division, Addition, and Subtraction)
	1. 3 * 4 = 12
	2. 12 / 5 = 2.4
	3. 1 + 2 = 3
	4. 3 - 2.4 = 0.6
*/

let mdas = 1 + 2 - 3 * 4 / 5;
console.log("Result of mdas operation: " + mdas);

let pemdas = 1 + (2 - 3) * (4 / 5);
console.log(pemdas);

// INCREMENT AND DECREMENT
// Operators that add or subtract values by 1 and reassigns the variable where the increment/decrement was applied to
let z = 1;

// Pre-Increment
let increment = ++z;
/*
	1. z = z + 1
	2. increment = z
*/
console.log("Result of the pre-increment: " + increment); //2

console.log("Result of the pre-increment: " + z); // 2

// Post-Increment 
// The value if "z" is returned and stored in the variable "postIncrement" then the value of "z" is increased by one
let w = 1;
let postIncrement = w++; 
/*
	1. postIncrement = w
	2. w = w + 1
*/
console.log("Result of the post-increment: " + postIncrement); // 1
console.log("Result of the post-increment: " + w); // 2

/*
	Pre-increment - adds 1 first before reading value
	Post-increment - reads value first before adding 1

	Pre-decrement - subtracts 1 first before reading value
	Post-decrement - reads value first before subtracting 1
*/

let a = 2;

// Pre-Decrement
// The value "a" is decreased by a value of 1 before returning the value and storing it in the variable "preDecrement"
let preDecrement = --a;
console.log("Result of the pre-decrement: " + preDecrement); // 1
console.log("Result of the pre-decrement: " + a); // 1

// Post-Decrement
// The value "a" is returned and stored in the variable postDecrement then the value is decreased by a value of 1
let postDecrement = a--;
console.log("Result of the postDecrement: " + postDecrement);
console.log("Result of the postDecrement: " + a);


// TYPE COERCION
/*
	Type coercion is the automatic or implicit conversion of values from one data type to another.
*/

let numA = '10'; // String
let numB = 12; // Number

/*
	Adding/concatenating a string and a number will result as a string
*/

let coercion = numA + numB;
console.log(coercion); // 1012
console.log(typeof coercion); // String

coercion = numA - numB;
console.log(coercion); // -2
console.log(typeof coercion); // Number

// Non-coercion
let numC = 16;
let numD = 14;

let nonCoercion = numC + numD;
console.log(nonCoercion);
console.log(typeof nonCoercion);


// Addition of Number and Boolean
/*
	The result is a number
	The boolean "true" is associated with the value of 1
	The boolean "false" is associated with the value of 0
*/

let numE = true + 1;
console.log(numE); // 2
console.log(typeof numE);


let numF = false + 1;
console.log(numF); // 1
console.log(typeof numF);


// COMPARISON OPERATOR
let juan = "juan";

// Equality Operator (==)
/*
	- Checks whether the operands are equal/have the same content
	- Attempts to CONVERT and COMPARE operands of different data types
	- Returns a boolean value (true / false)
*/

console.log(1 == 1); // true
console.log(1 == 2); // false
console.log(1 == '1'); // true
console.log(1 == true); // true

// compare two strings that are the same
console.log('juan' == 'juan'); // true
console.log('true' == true); // false -> NaN == 1
console.log(juan == 'juan'); // true

// Inequality Operator (!=)
/*
	- Checks whether the operands are not equal/have different content
	- Attempts to CONVERT and COMPARE operands of different data types
*/

console.log(1 != 1); // false
console.log(1 != 2); // true
console.log(1 != '1'); // false
console.log(1 != true); // false
console.log('juan' != 'juan'); // false
console.log('juan' != juan); // false


// Strict Equality Operator (===)
/*
	- Checks whether the operands are equal/have the same content
	- Also COMPARES the data types of 2 values
*/

console.log(1 === 1); // true
console.log(1 === 2); // false
console.log(1 === '1'); // false
console.log(1 === true); // false
console.log('juan' === 'juan'); // true
console.log(juan === 'juan'); // true

// Strict Inequality Operator (!==)
/*
	- Checks whether the operands are not equal/have the same content
	- Also COMPARES the data types of 2 values
*/

console.log(1 !== 1); // false
console.log(1 !== 2); // true
console.log(1 !== '1'); // true
console.log(1 !== true); // true
console.log('juan' !== 'juan'); // false
console.log(juan !== 'juan'); // false


// RELATIONAL OPERATOR
// Returns a boolean value
let j = 50;
let k = 65;

// Greater than operator (>)
let isGreaterThan =  j > k;
console.log(isGreaterThan); // false

// Less than operator (<)
let isLessThan = j < k;
console.log(isLessThan); // true

// Greater than or Equal operator (>=)
let isGTorEqual = j >= k;
console.log(isGTorEqual); // false

// Less than or Equal operator (<=)
let isLTorEqual = j <= k;
console.log(isLTorEqual); // true

let numStr = "30";
// forced coercion to change the string to a number
console.log(j > numStr); // true -> j > 30

let str = "thirty";
// 50 is greater than NaN
console.log(j > str); // false -> j > NaN


// LOGICAL OPERATORS
let isLegalAge = true;
let isRegistered = false;

// Logical AND Operator (&&)
// Returns true if all operands are true
// true && true = true
// true && false = false

let allRequirementsMet = isLegalAge && isRegistered;
console.log(allRequirementsMet); // false

// Logical OR operator (||)
// Returns true if one of the operands are true
// t || t = t
// t || f = t
// f || f = f

let someRequirementsMet = isLegalAge || isRegistered;
console.log(someRequirementsMet); // true

// Logical NOT operator (!)
// Returns the opposite value
let someRequirementsNotMet = !isRegistered;
console.log(someRequirementsNotMet);